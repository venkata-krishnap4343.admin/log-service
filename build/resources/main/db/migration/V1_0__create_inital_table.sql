create table if not exists books_log(
    id INTEGER NOT NULL AUTO_INCREMENT,
    message varchar(255) NOT NULL,
    time_stamp varchar(225) NOT NULL,
    PRIMARY KEY (id)
);

create table if not exists customer_log(
    id INTEGER NOT NULL AUTO_INCREMENT,
    message varchar(255) NOT NULL,
    time_stamp varchar(225) NOT NULL,
    PRIMARY KEY (id)
);