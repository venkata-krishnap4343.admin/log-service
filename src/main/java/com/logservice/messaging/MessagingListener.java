package com.logservice.messaging;

import com.logservice.model.Message;
import com.logservice.service.BookLogService;
import com.logservice.service.CustomerLogService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Slf4j
public class MessagingListener {

    BookLogService bookLogService;
    CustomerLogService customerLogService;

    @RabbitListener(queues = "book_log_queue")
    public void logBook(Message message){
        bookLogService.logBookRequest(message);
    }

    @RabbitListener(queues = "customer_log_queue")
    public void logCustomer(Message message){
        customerLogService.logCustomerRequest(message);
    }


}
