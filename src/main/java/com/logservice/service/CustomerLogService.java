package com.logservice.service;

import com.logservice.Dao.CustomerMessageDao;
import com.logservice.model.CustomerMessageEntity;
import com.logservice.model.Message;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class CustomerLogService {
    CustomerMessageDao customerMessageDao;
    public void logCustomerRequest(Message message) {
        customerMessageDao.save(CustomerMessageEntity.builder().message(message.getMessage()).timeStamp(message.getTime()).build());
    }

    public List<CustomerMessageEntity> getCustomerLogs() {
        return customerMessageDao.findAll();
    }
}
