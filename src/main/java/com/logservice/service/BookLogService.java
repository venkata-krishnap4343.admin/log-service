package com.logservice.service;

import com.logservice.Dao.BookMessageDao;
import com.logservice.model.Message;
import com.logservice.model.BooksMessageEntity;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class BookLogService {

    BookMessageDao bookMessageDao;

    public void logBookRequest(Message message) {
        bookMessageDao.save(BooksMessageEntity.builder().message(message.getMessage()).timeStamp(message.getTime()).build());
    }

    public List<BooksMessageEntity> getAllLogs() {
        return bookMessageDao.findAll();
    }
}
