package com.logservice.Dao;

import com.logservice.model.CustomerMessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerMessageDao extends JpaRepository<CustomerMessageEntity, Integer> {
}
