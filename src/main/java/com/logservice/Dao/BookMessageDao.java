package com.logservice.Dao;

import com.logservice.model.BooksMessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookMessageDao extends JpaRepository<BooksMessageEntity, Integer> {
}
