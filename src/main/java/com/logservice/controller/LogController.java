package com.logservice.controller;

import com.logservice.model.BooksMessageEntity;
import com.logservice.model.CustomerMessageEntity;
import com.logservice.service.BookLogService;
import com.logservice.service.CustomerLogService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/log/service")
@AllArgsConstructor
public class LogController {

    BookLogService bookLogService;
    CustomerLogService customerLogService;

    @GetMapping(path = "/get-books-logs", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BooksMessageEntity> getBooksLogs(){
        return bookLogService.getAllLogs();
    }

    @GetMapping(path = "/get-customer-log", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CustomerMessageEntity> getCustomerLogs(){
        return customerLogService.getCustomerLogs();
    }
}
